#! python3
# trim_panorama.py
# by Tim Littlefair.  Copyleft/license TBD.
# Script to fix up panoramas taken by Nikon AW110 and 
# similar digital cameras which contain a grey area at one 
# end.

import os
import sys

from PIL import Image

BLACK_PIXEL = ( 40, 40, 40 )

def crop_on_right(image):
    hsize, vsize = image.size
    crop_width_lbound = 0
    crop_width_ubound = hsize - 1
    while crop_width_ubound > crop_width_lbound:
        new_bound = int ( (crop_width_lbound + crop_width_ubound)/2 )
        if new_bound in (crop_width_ubound, crop_width_lbound):
            break
        top_pixel = image.getpixel((new_bound,vsize-1,))
        bottom_pixel = image.getpixel((new_bound,0,))
        if top_pixel == BLACK_PIXEL and bottom_pixel == BLACK_PIXEL:
            crop_width_ubound = new_bound
        else:
            crop_width_lbound = new_bound
    # At this point we know that the lower and upper bounds for the 
    # crop width differ by 1 or less, and that the last non-black 
    # column index is one of these values
    top_pixel = image.getpixel((crop_width_ubound,vsize-1,))
    bottom_pixel = image.getpixel((crop_width_ubound,0,))
    if top_pixel == BLACK_PIXEL and bottom_pixel == BLACK_PIXEL:
        crop_width_lbound = crop_width_ubound
    else:
        crop_width_ubound = crop_width_lbound
    # At this point the upper and lower bounds are identical 
    print("new width %d"%(crop_width_lbound,))
    return image.crop((0,0,crop_width_lbound,vsize-1,))

def get_paths (
    image_path, original_basename_suffix, fixed_basename_suffix
):
    basename, extension = os.path.splitext(image_path)
    original_rename_path = None
    if original_basename_suffix is not None:
        original_rename_path = "%s%s%s" % (
            basename, original_basename_suffix, extension
        )
    fixed_path  = "%s%s%s" % (
        basename, fixed_basename_suffix, extension
    )
    return original_rename_path, fixed_path

def fixup_panorama_blackness(
    image_path, 
    original_basename_suffix="-original",
    fixed_basename_suffix=""
):
    original_renamed_path, fixed_path = get_paths (
        image_path, original_basename_suffix, fixed_basename_suffix
    )
    try:
        original_image = Image.open(image_path)
    except IOError:
        print("Ignoring unsupported file "+image_path)
        return
    hsize, vsize = original_image.size
    print("Image %s is %d x %d pixels" % (image_path,hsize,vsize,),end=", ")
    CORNERS = (
        ( "top right", hsize-1, 0, ),
        ( "bottom left", 0, vsize-1,),
        ( "bottom right", hsize-1, vsize-1)
    )
    for c in CORNERS:
        continue # Comment this line out to allow the logging below to appear
        print("%s pixel is %s" % (
             c[0], original_image.getpixel(c[1:]),
        ))
    if original_image.getpixel(CORNERS[2][1:]) != BLACK_PIXEL:
        print("does not require fixing")
    elif original_image.getpixel(CORNERS[0][1:]) == BLACK_PIXEL:
        print("requires cropping on the right",end=", ")
        if original_renamed_path is None:
            os.remove(image_path)
        elif original_renamed_path != image_path:
            os.rename(image_path, original_renamed_path)
        cropped_image = crop_on_right(original_image)
        cropped_image.save(fixed_path,quality=95,exif=cropped_image.info['exif'])
    elif original_image.getpixel(CORNERS[1][1:]) == BLACK_PIXEL:
        print("requires cropping at the bottom (not yet supported)")
    else:
        print("does not require fixing")
    return

def process_path_item(
    path,
    original_basename_suffix="-original",
    fixed_basename_suffix=""
):
    if os.path.isdir(path):
        for f in os.listdir(path):
            fpath = os.path.join(path,f)
            process_path_item(
                fpath,
                original_basename_suffix, 
                fixed_basename_suffix 
            )
    else:
        fixup_panorama_blackness(
            path,
            original_basename_suffix, 
            fixed_basename_suffix 
        )

if __name__ == "__main__":
    if len(sys.argv)==1:
        TEST_FILE="DSCN0562.JPG"
        fixup_panorama_blackness(TEST_FILE,"","-fixed")
    else:
        for arg in sys.argv[1:]:
            process_path_item(arg,None,"")
            
